---
layout: article
title: "responsive web design"
date: 2018-6-11
categories:
  - Network and New Media
---







# 响应式网页设计
##  名词解释

1 是一种网页设计的技术做法，该设计可使网站在多种浏览设备（从桌面电脑显示器到移动电话或其他流动产品装置）上阅读和导航，同时减少缩放、平移和滚动。 via 知乎

2 内容布局能随用户使用显示器的不同而变化。 via百度

3网页内容会随着访问它的视口及设备的不同而呈现不同的样式。via课本


- 视口(viewport):浏览器中用于呈现网页的区域
- 断点( breakpoint )：某个宽度临界点，跨过这个点布局就会发生显著变化。断点应该由你自己的项目设计决定。
- 前端(frontend):指网站的前台部分。网站的视觉设计，应用产品是用户看到，接触到和体验到，他们主要做静态用户界面加上一些动态效果，不涉及数据逻辑，前端考虑到的是用户体验
- 后端(backend):程序设计架构思想，管理数据库。
-  弹性布局：百分比布局使得网页宽度能够随着查看他们的屏幕窗口大小变化
- Internet Explorer 11（简称IE11）是微软开发网页浏览器，是Internet Explorer 10的下一代    
- 集成开发环境（IDE，Integrated Development Environment ）是用于提供程序开发环境的应用程序，一般包括代码编辑器、编译器、调试器和图形用户界面等工具。
- 文本编辑器
1.	Microsoft Visual Studio（简称VS）是美国微软公司的开发工具包系列产品。
2.	Sublime Text 是一个代码编辑器也是HTML和散文先进的文本编辑器。它最初被设计为一个具有丰富扩展功能的Vim。
3.	Coda是Panic推出适用在Mac上的网页开发工具
4.	notepad（记事本）是代码编辑器或WINDOWS中的小程序，用于文本编辑
- CSS预处理器：基于 CSS 扩展了一套属于自己的 DSL，来解决我们书写 CSS 时难以解决的问题。手工做的事自动化
##  三项组合技术
- 弹性图片( fluid grid):不给图片设置固定尺寸，而是根据流体网格进行缩放，用于适应各种网格的尺寸。img {max-width:100%;}
- 弹性网格布局(fluid grid)：给予容器控制内部元素高度和宽度的能力，利用 例如利用< div class="div" style="flex-basis:80px">项目在主轴上占据的空间，定义表格为80像素
- 媒体查询(media queries)
功能
> 1 媒体类型和零个或多个检测媒体特性的表达式。
2 可以不必修改内容本身，而让网页适配不同的设备。
3 具有在CSS中实现条件逻辑的能力
4 可以从整体上修改一个网站的布局和外观

标签
<link> 询问设备的类型和特性
<meta>可以在其中设置像素
## 学习Marcotte
- 我建立了一个简单的页面为一个假设的杂志;这是一个建立在一个简单的两列布局流体网格不少灵活的图像的。——弹性图片
- 我们可以设计一个最佳的观看体验,但基于标准的技术嵌入到我们的设计不仅让他们更灵活,更适应媒体呈现。——所谓”渐进增强“
- 媒体查询允许我们目标不仅是某些设备类,但实际检查设备的物理特性呈现我们的工作——即是又媒体类型和媒体特征

## 为何需要响应式设计
- statcounter分析
Screen Resolution Stats Worldwide
（时间：2017年2月 - 2018年2月）

|分辨率|百分率范围|对应产品|
|---------|--------|------------|
|360×640|17.91%~23.89%|4.7寸手机
|1366×768|11.89%~13.34%|电脑|
|1920×1080|6.62%~7.88%|电脑|
![image.png](https://upload-images.jianshu.io/upload_images/8893975-ec9413ff360eb7f8.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


- 结论：
1.这个表格是屏幕分辨率是全世界使用频率排名前三的三种。从对应的产品生产来看使用手机频率是最高的。但后两者都是电脑。从而看出当今人们的生活总在手机与电脑中切换。而如果你设计的页面只适应在电脑上才有较好的既视感。而在手机上需要用户自行拖拉，排版很乱。并且在这种盛行使用手机的时代，那岂不是会让人舍弃你的产品。所以你需要设计一个可以即满足电脑又满足手机的平面设计
2.如果我们采用后端那样先把可能呈现的平面都设计出来，这样很费时间。但是响应式设计可以让前端人员只需要开发一次就好，pc、触屏、手机版都可以使用。这样既有效 率，又可以让网站的布局和功能随用户的使用环境而变化
## 例子
- 运用响应式设计http://packdog.com/preview
菜单会在上方，当到达632×787这一断点后会把菜单隐藏。图片先才有浮动排至最右边经过断点后图片会变成符合手机滑动方式单张如杂志般滚动
- 没有使用响应式设计 http://m.wufazhuce.com/
- 比较

|网站|菜单|图片布局|
|------|------|-----------|
|http://packdog.com/preview|电脑视觉下菜单在上方，到达32×787断点后会把菜单隐藏|浮动排至最右边经过断点后图片会变成符合手机滑动方式单张如杂志般滚动|
| http://m.wufazhuce.com/|菜单从始至终采用隐藏的方式|图片在手机上是符合视觉下单张图片观赏，但没有断点，直到1085×526时照片像被过度拉长，整个页面布局严重拉伸。字也变得很小看不清晰|
- 效果
运用响应式前后比较![image.png](https://upload-images.jianshu.io/upload_images/8893975-3cd6e8ca8798d131.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)  
 ![image.png](https://upload-images.jianshu.io/upload_images/8893975-649957da753e4bb3.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

不运用响应式前后比较![image.png](https://upload-images.jianshu.io/upload_images/8893975-f52f1860009ee8d7.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
![image.png](https://upload-images.jianshu.io/upload_images/8893975-19173ac7212f72ac.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)



##  响应式与前后端关系
- 访问者(visitors)会在前端网站输入用户信息，前端就会把client(顾客）的要求通过网络服务（web services）传给后端，后端的工作人员(staff)便会在数据库（data base)里找到符合顾客要求的东西再传致网络服务供用户使用。
>API可以简单的理解为一个URL地址。具体来说是前端页面向API发送了一个请求的数据A，后端接收到A，根据业务的需求将A变成处理后的数据B，并把B返回给前端页面上。这个过程中前端不知道也不必知道API内部是如何工作的，前端只需要根据发送的数据获取到需要的数据。后端的工作就是接收前端发来的数据，处理后返回给前端使用。

- https://www.jianshu.com/p/84ea06be0278 前后端
- https://www.imooc.com/wenda/detail/271938
- http://blog.csdn.net/jiangbo_phd/article/details/45501151 弹性图片
- https://www.cnblogs.com/nuannuan7362/p/5823381.html flex弹性布局
- https://alistapart.com/article/responsive-web-design  Marcotte对响应式介绍
