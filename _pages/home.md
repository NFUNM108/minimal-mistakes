---
layout: splash
permalink: /
header:
  overlay_color: "#5e616c"
  overlay_image: /assets/images/home_header.jpg
  cta_label: "<i class='fas fa-download'></i> Install Now"
  cta_url: "/docs/quick-start-guide/"
  caption:
excerpt: '也许每个人都是幸运的皮卡丘'.<br /> <small><a href="https://github.com/mmistakes/minimal-mistakes/releases/tag/4.10.1">Latest release v4.11.2</a></small><br /><br /> {::nomarkdown}<iframe style="display: inline-block;" src="https://ghbtns.com/github-btn.html?user=mmistakes&repo=minimal-mistakes&type=star&count=true&size=large" frameborder="0" scrolling="0" width="160px" height="30px"></iframe> <iframe style="display: inline-block;" src="https://ghbtns.com/github-btn.html?user=mmistakes&repo=minimal-mistakes&type=fork&count=true&size=large" frameborder="0" scrolling="0" width="158px" height="30px"></iframe>{:/nomarkdown}'
feature_row1:
  - image_path: /assets/images/简书_home.jpg
    alt: "简书"
    title: "简书"
    excerpt: '个人学习笔记发表平台，关于网络与新媒体关键词等知识'
    url: "https://www.jianshu.com/u/f4d073d8c765"
    btn_label: "Read More"
    btn_class: "btn--primary"
feature_row2:
  - image_path: /assets/images/svg_home.jpg
    alt: "svg制作"
    title: "svg制作"
    excerpt: '关于svg元素的认识，css动画和svg动画的制作'
    url: "svg"
    btn_label: "Read More"
    btn_class: "btn--primary"
feature_row3:
  - image_path: /assets/images/interface_header.jpg
    alt: "界面设计"
    title: "界面设计"
    excerpt: '导航栏的认识与设置，图标的运用以及Jekyll的介绍'
    url: "design/#界面设计"
    btn_label: "Read More"
    btn_class: "btn--primary"
feature_row4:
  - image_path: /assets/images/graphic_home.jpg
    alt: "平面设计"
    title: "平面设计"
    excerpt: '网页页面颜色搭配，css滤镜的使用以及衬线字体和无衬线字体的学习'
    url: "design/#平面设计"
    btn_label: "Read More"
    btn_class: "btn--primary"
feature_row5:
  - image_path: /assets/images/about_home.jpg
    alt: "关于我"
    title: "关于我"
    excerpt: '个人的相关信息'
    url: "about"
    btn_label: "Read More"
    btn_class: "btn--primary"    
github:
  - excerpt: '{::nomarkdown}<iframe style="display: inline-block;" src="https://ghbtns.com/github-btn.html?user=mmistakes&repo=minimal-mistakes&type=star&count=true&size=large" frameborder="0" scrolling="0" width="160px" height="30px"></iframe> <iframe style="display: inline-block;" src="https://ghbtns.com/github-btn.html?user=mmistakes&repo=minimal-mistakes&type=fork&count=true&size=large" frameborder="0" scrolling="0" width="158px" height="30px"></iframe>{:/nomarkdown}'
intro:
  - excerpt: 'Get notified when I add new stuff &nbsp; [<i class="fab fa-twitter"></i> @mmistakes](https://twitter.com/mmistakes){: .btn .btn--twitter} [<i class="fab fa-paypal"></i> Tip Me](https://www.paypal.me/mmistakes){: .btn .btn--primary}'
---

{% include feature_row %}

{% include feature_row id="feature_row1" type="left" %}

{% include feature_row id="feature_row2" type="right" %}

{% include feature_row id="feature_row3" type="left" %}

{% include feature_row id="feature_row4" type="right" %}

{% include feature_row id="feature_row5" type="center" %}
