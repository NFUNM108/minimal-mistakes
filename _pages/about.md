---
title: "about"
permalink: /about/
date: 2018-07-03
gallery:
  - url: /assets/images/red_flower.JPG
    image_path: /assets/images/red_flower.JPG
    alt: "red flower"
    title: "red flower"
  - url: /assets/images/pink_flower.JPG
    image_path: /assets/images/pink_flower.JPG
    alt: "pink flower"
    title: "Image 2 title caption"
  - url: /assets/images/leave.JPG       
    image_path: /assets/images/leave.JPG
    alt: "strawberry"
    title: "strawberry"
  - url: /assets/images/children.JPG         
    image_path: /assets/images/children.JPG
    alt: "children"
    title: "children"
  - url: /assets/images/beautiful.JPG         
    image_path: /assets/images/beautiful.JPG
    alt: "girl"
    title: "girl"
  - url: /assets/images/girl.JPG         
    image_path: /assets/images/girl.JPG
    alt: "girl"
    title: "girl"

---


## 个人信息
- 姓名：周滋娴
- 性别：女
- 籍贯：汉族
- 年龄：19岁


## 教育经历
- 潮州金山中学2015.9-2017.6
- 中山大学南方学院 2017.9 - 至今——网络与新媒体
